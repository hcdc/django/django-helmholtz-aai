.. SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _strategies:

User creation strategies
========================

This package provides various strategies to customize how users from the
Helmholtz AAI are added to the application. As soon as a user has been added,
we save the unique identifier (``eduperson_unique_id``) and the user will be
mapped to the same django user on each login via the helmholtz AAI.

However, the question is how users are added to the application. We face two
challenges here:

1. how to map users from the helmholtz AAI to existing users in Django (in case
   your application existed already prior to using `django-helmholtz-aai`)
2. how to handle the change of login providers in the helmholtz AAI? One person
   can have multiple accounts in the Helmholtz AAI (one via OrcID, one via
   GitHub). So what shall we do, when the user logs in from different accounts
   in the Helmholtz AAI

There are a variety of strategies to tackle these two challenges and we
desribe them in the following section.

Core concept
------------
We distinguish two major cases:

1. the email of the user does not exist in our database

   When the email exists already, we have the following options:

   ``create-new``
       We create a new user of the application
   ``no-new``
       We tell the user that a login is not
   ``manual``
       We ask the user to tell us an existing email in the application and verify
       the login

2. the email of the user exists already in our database

   When the associated user account already has an associated
   ``eduperson_unique_id``, we can either

   - update this ``eduperson_unique_id`` and delete the old one,
   - create an entirely new account,
   - let the user decide, whether he wants to update the existing account or
     create a new one,
   - or prevent the user from logging in.

   When the user does **not** already have an associated ``eduperson_unique_id``,
   we can either

   - associate the new ``eduperson_unique_id`` with the django user,
   - prevent the login,
   - let the user decide, whether he wants to update the existing account or
     create a new one,
   - or create an entirely new account.


A combination of these choices describe the strategy that you can set via the
:setting:`HELMHOLTZ_CREATE_USERS_STRATEGY`. We do not yet cover all
possible combinations of the above scenarios. In general, the strategy is a
combination of up to three strings, the default is
``["create-new", "no-map", "no-duplicated-helmholtz"]``, which implements that
we create new users, but we prevent creation of new users when there is already
a user with the same email. Another common strategy will be simply
``["create-new"]`` which will always create new user accounts for each user in
the Helmholtz AAI.


.. _implemented-strategies:

Implemented strategies
----------------------

Not all strategies are implemented yet, but the following setups can be used:

.. list-table:: Implemented strategies
    :header-rows: 1

    * - :setting:`HELMHOLTZ_CREATE_USERS_STRATEGY`
      - Description
      - Implementation
    * - .. code-block:: python

            'create-new'

      - always create new users:

        - when the email exists already:
            - create new user
        - when the email does not exist:
            - create new user
      - :mod:`django_helmholtz_aai.views.auth.create_new`


    * - .. code-block:: python

            'create-new',
            'no-duplicated-helmholtz',
            'no-map'

      - Create new users, when the email does not yet exist

        - when the email exists already:
            - prevent login
        - when the email does not exist:
            - create new user

      - :mod:`django_helmholtz_aai.views.auth.create_new_no_map_no_duplicated_helmholtz`


    * - .. code-block:: python

            'create-new',
            'duplicate-helmholtz',
            'map-existing'

      - Map helmholtz users to existing users, but create new users when they
        have been mapped already

        - when the email exists already:
            - when the user with the mail already has a helmholtz user:
                - create new user
            - when the user with the mail does not have a helmholtz user:
                - map user
        - when the email does not exist:
            - create new user

      - :mod:`django_helmholtz_aai.views.auth.create_new_map_existing_duplicate_helmholtz`


    * - .. code-block:: python

            'create-new',
            'map-existing',
            'remap-helmholtz'

      - Map helmholtz users to existing users, and update the user when the
        user logs in from another account in the Helmholtz AAI

        - when the email exists already:
            - when the user with the mail already has a helmholtz user:
                - map user
            - when the user with the mail does not have a helmholtz user:
                - map user
        - when the email does not exist:
            - create new user

      - :mod:`django_helmholtz_aai.views.auth.create_new_map_existing_remap_helmholtz`


    * - .. code-block:: python

            'create-new',
            'duplicate-helmholtz',
            'no-map'

      - Prevent an update of existing users, but allows duplicated users in the
        Helmholtz AAI

        - when the email exists already:
            - when the user with the mail already has a helmholtz user:
                - create new user
            - when the user with the mail does not have a helmholtz user:
                - prevent login
        - when the email does not exist:
            - create new user

      - :mod:`django_helmholtz_aai.views.auth.create_new_no_map_duplicate_helmholtz`


    * - .. code-block:: python

            'manual-new',
            'map-existing',
            'no-duplicated-helmholtz'

      - Map users where possible, or let the user decide, whether he wants to
        update an existing user. But prevent an update of existing helmholtz
        users.

        - when the email exists already:
            - when the user with the mail already has a helmholtz user:
                - prevent login
            - when the user with the mail does not have a helmholtz user:
                - map user
        - when the email does not exist:
            - update an existing user (verification link)

      - :mod:`django_helmholtz_aai.views.auth.manual_new_map_existing_no_duplicated_helmholtz`


    * - .. code-block:: python

            'no-new'

      - Prevent any unknown user

        - when the email exists already:
            - prevent login
        - when the email does not exist:
            - prevent login

      - :mod:`django_helmholtz_aai.views.auth.no_new`


    * - .. code-block:: python

            'map-existing',
            'no-duplicated-helmholtz',
            'no-new'

      - Prevent new users or duplicated helmholtz accounts, but map where
        possible

        - when the email exists already:
            - when the user with the mail already has a helmholtz user:
                - prevent login
            - when the user with the mail does not have a helmholtz user:
                - map user
        - when the email does not exist:
            - prevent login

      - :mod:`django_helmholtz_aai.views.auth.no_new_map_existing_no_duplicated_helmholtz`


    * - .. code-block:: python

            'map-existing',
            'no-new',
            'remap-helmholtz'

      - Prevent new users, map where possible and update existing helmholtz
        accounts

        - when the email exists already:
            - when the user with the mail already has a helmholtz user:
                - remap user (update ``eduperson_unique_id``)
            - when the user with the mail does not have a helmholtz user:
                - map user
        - when the email does not exist:
            - prevent login

      - :mod:`django_helmholtz_aai.views.auth.no_new_map_existing_remap_helmholtz`


    * - .. code-block:: python

            'duplicate-helmholtz',
            'no-map',
            'no-new'

      - Prevent new users or updating existing standard django users, but allow
        creating new helmholtz users for existing helmholtz members

        - when the email exists already:
            - when the user with the mail already has a helmholtz user:
                - create new user
            - when the user with the mail does not have a helmholtz user:
                - prevent login
        - when the email does not exist:
            - prevent login

      - :mod:`django_helmholtz_aai.views.auth.no_new_no_map_duplicate_helmholtz`
