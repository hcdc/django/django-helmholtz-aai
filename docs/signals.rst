.. SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _configure-signals:

Configuration via Signals
^^^^^^^^^^^^^^^^^^^^^^^^^

The :mod:`~django_helmholtz_aai.signals` module defines various signal that are
fired on different events:

.. automodulesumm:: django_helmholtz_aai.signals
    :autosummary-no-titles:
    :autosummary-imported-members:
    :autosummary-exclude-members: Signal

The purpose of these signals should be pretty much self-explanatory.

Examples
~~~~~~~~
Suppose you want users of a specific VO to become superusers. Then you can do
something like this using the :signal:`aai_vo_entered` and
:signal:`aai_vo_left` signals::

    from django.dispatch import receiver

    from django_helmholtz_aai import models, signals

    @receiver(signals.aai_vo_entered)
    def on_vo_enter(
            sender,
            vo: models.HelmholtzVirtualOrganization,
            user: models.HelmholtzUser,
            **kwargs,
        ):
        vo_id = "urn:geant:helmholtz.de:group:hereon#login.helmholtz.de"
        if vo.eduperson_entitlement == vo_id:
            user.is_superuser = True
            user.save()


    @receiver(signals.aai_vo_left)
    def on_vo_leave(
            sender,
            vo: models.HelmholtzVirtualOrganization,
            user: models.HelmholtzUser,
            **kwargs,
        ):
        vo_id = "urn:geant:helmholtz.de:group:hereon#login.helmholtz.de"
        if vo.eduperson_entitlement == vo_id:
            user.is_superuser = False
            user.save()

Let's say you want to display a message in the frontend when a user logged in
for the first time. Here you can use the :signal:`aai_user_created` signal::

    from django.contrib import messages

    from django_helmholtz_aai import models, signals

    @receiver(signals.aai_user_created)
    def created_user(
        sender,
        user: models.HelmholtzUser,
        request,
        **kwargs,
    ):
        messages.add_message(
            request, messages.success, f"Welcome on board {user}!"
        )
