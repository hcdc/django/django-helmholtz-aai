# SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for django-helmholtz-aai."""
from __future__ import annotations

from typing import (
    TYPE_CHECKING,
    Any,
    Callable,
    Optional,
    Protocol,
    Type,
    TypeVar,
    Union,
    cast,
)
from unittest import mock
from uuid import uuid4

import pytest  # noqa: F401
from django.contrib.auth.middleware import AuthenticationMiddleware
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.core.handlers.wsgi import WSGIRequest
from django.utils.functional import cached_property
from django.views.generic import View

if TYPE_CHECKING:
    from django.test import RequestFactory


@pytest.fixture
def userinfo() -> dict[str, Any]:
    uuid = str(uuid4())

    return {
        "sub": "bdeba218-2342-3456-sad3-ff4sdfew2444",
        "email_verified": True,
        "name": "Firstname Lastname",
        "eduperson_unique_id": uuid + "@login.helmholtz-data-federation.de",
        "preferred_username": uuid,
        "given_name": "Firstname",
        "family_name": "Lastname",
        "email": uuid + "@example.com",
        "eduperson_entitlement": [
            "urn:geant:helmholtz.de:group:some_VO#login.helmholtz.de",
            "urn:geant:helmholtz.de:group:some_VO:subgroup#login.helmholtz.de",
            "urn:mace:dir:entitlement:common-lib-terms",
        ],
    }


@pytest.fixture
def username(userinfo: dict[str, Any]) -> str:
    return userinfo["preferred_username"]


class PatchedAuthentificationViewMixin:
    """Patched authentification view as we cannot test against the real AAI."""

    _userinfo: dict[str, Any]

    raise_exception = True

    @cached_property
    def userinfo(self) -> dict[str, Any]:
        return self._userinfo


T = TypeVar("T", bound=View)


class _CreateView(Protocol):
    def __call__(
        self, cls_: Type[T], request: Optional[Union[str, WSGIRequest]] = None
    ) -> T:
        ...


@pytest.fixture
def setup_authentification_view(
    db, rf: RequestFactory, userinfo: dict[str, Any]
) -> _CreateView:
    """A factory for creating an authentification view."""

    def create_view(
        cls_: Type[T], request: Optional[Union[str, WSGIRequest]] = None
    ) -> T:
        if request is None:
            request = rf.get("/helmholtz-aai/")
        elif isinstance(request, str):
            request = rf.get(request)

        cast(WSGIRequest, request)

        # Passing None as the first argument to MiddlewareMixin.__init__() (which
        # is used by Session-, Authentication- and Message Middleware) is
        # deprecated from Django 3.1
        # (https://docs.djangoproject.com/en/4.2/releases/3.1/#id2) and removed
        # in Django 4.0
        # (https://docs.djangoproject.com/en/4.2/releases/4.0/#features-removed-in-4-0)
        # so it has to be mocked in this place
        get_response = mock.MagicMock()

        session_middleware = SessionMiddleware(get_response)
        session_middleware.process_request(request)
        request.session.save()

        auth_middleware = AuthenticationMiddleware(get_response)
        auth_middleware.process_request(request)

        message_middleware = MessageMiddleware(get_response)
        message_middleware.process_request(request)

        class PatchedAuthentificationView(
            PatchedAuthentificationViewMixin, cls_  # type: ignore[valid-type, misc]
        ):
            pass

        view = PatchedAuthentificationView()
        view._userinfo = userinfo
        view.setup(request)
        return view

    return create_view


@pytest.fixture
def patched_signals(monkeypatch) -> list[str]:
    """Patched signals."""
    from django_helmholtz_aai import signals

    signals_raised = []

    def send_factory(signal: str) -> Callable:
        def send(*args, **kwargs):
            signals_raised.append(signal)

        return send

    for signal in [
        "aai_user_created",
        "aai_user_logged_in",
        "aai_user_updated",
        "aai_vo_created",
        "aai_vo_entered",
        "aai_vo_left",
    ]:
        monkeypatch.setattr(
            getattr(signals, signal), "send", send_factory(signal)
        )
    return signals_raised
